#Name: Sam LeCompte
#Date: 10/15/15
#Project: Trivia Project

import question

def setPlayerPoints():
    pList = list()
    numPlayers = int(input("How many players are there?  "))
    for x in range(0,numPlayers):
        pList.append(0)
    return pList
        

playerList = setPlayerPoints()

qList = open("questionTxt.txt",'r')
questionList = list()
questionLine = qList.readline()


while(questionLine!="\n"):
    qParts = questionLine.split('|')
    questionList.append(question.Question(qParts[0],qParts[1],qParts[2],qParts[3],qParts[4],int(qParts[5])))
    questionLine = qList.readline()  
   
turn = 0

for q in questionList:
    q.printQuestion()
    answer = int(input("Take a guess between 1 and 4 (i.e. Player " + str(turn+1) + "):  "))
    if(q.isCorrect()==answer):
        print("There you go G!")
        playerList[turn] += 1
    else:
        print("Sorry B.")
        
    if(turn < len(playerList)-1):
        turn += 1
    else:
        turn = 0


print(playerList)

highestPoints = 0
winner = 0
index =0
for points in playerList:
    if(points > highestPoints):
        highestPoints = points
        winner = index
    index += 1

print("Player", winner+1, "is the winner with", highestPoints, "points")

